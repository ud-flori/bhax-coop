public class IntegerCollection {

    int array[];
    int size;
    boolean sorted = false;
    int index = 0;



    public IntegerCollection(int size){
        this.size = size;
        this.array = new int[size];
    }

    public IntegerCollection(int[] array){
        this.size = array.length;
        this.array = array;
        this.index = size;
    }

    public void add(int value){
        if(index>=size){
            throw new IllegalArgumentException("The collection is full.");
        }
        sorted = false;
        array[index++] = value;
        }

    public boolean contain(int value){
        if(sorted == false){
            sort();
        }
        int left = 0;
        int right = size-1;
        while(left<=right){
            int mid = left + (right-left) /2;
        if(array[mid]==value){
            return true;
        }
        if(value<array[mid]){
            right = mid-1;
        }
        else{
            left = mid+1;
        }

        }
        return false;
    }

    public int[] sort() {
        for (int i = 0; i < size - 1; i++) {
            for (int j = 0; j < size - i - 1; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
        sorted = true;
        return array;
    }


}

