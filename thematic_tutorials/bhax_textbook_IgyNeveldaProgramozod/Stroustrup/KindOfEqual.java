public class KindOfEqual {
    /**
     * # Kind of equal
     *
     * Adott az alábbi kódrészlet.
     *
     * ```
     * // Given
     * String first = "...";
     * String second = "...";
     * String third = "...";
     *
     * // When
     * var firstMatchesSecondWithEquals = first.equals(second);
     * var firstMatchesSecondWithEqualToOperator = first == second;
     * var firstMatchesThirdWithEquals = first.equals(third);
     * var firstMatchesThirdWithEqualToOperator = first == third;
     *
     * ```
     *
     * Változtasd meg a ```String third = "...";``` sort úgy, hogy a firstMatchesSecondWithEquals,
     * firstMatchesSecondWithEqualToOperator, firstMatchesThirdWithEquals értéke true,
     * a firstMatchesThirdWithEqualToOperator értéke pedig false legyen. Magyarázd meg, mi történik a háttérben.
     */

    public static void main(String[] args){

        String first = "...";
        String second = "...";
        String third = new String("...");

        var firstMatchesSecondWithEquals = first.equals(second);
        var firstMatchesSecondWithEqualToOperator = first == second;
        var firstMatchesThirdWithEquals = first.equals(third);
        var firstMatchesThirdWithEqualToOperator = first == third;
        System.out.println(firstMatchesSecondWithEquals);
        System.out.println(firstMatchesSecondWithEqualToOperator);
        System.out.println(firstMatchesThirdWithEquals);
        System.out.print(firstMatchesThirdWithEqualToOperator);
    }
}
