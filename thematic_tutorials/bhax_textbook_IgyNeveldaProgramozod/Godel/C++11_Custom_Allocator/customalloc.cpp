#include <iostream>
#include <string>
#include <vector>

using namespace std;

template <typename T>
class CustomAlloc
{
public:
    using size_type = size_t;
    using pointer = T *;
    using const_pointer = const T *;
    using value_type = T;

    pointer allocate(size_type n)
    {
        cout << "Allocating "
             << n << " objects of "
             << n * sizeof(value_type)
             << " bytes. " << endl;
        return reinterpret_cast<pointer>(new char[n * sizeof(value_type)]);
    }

    void deallocate(pointer p, size_type n)
    {
        cout << "Deallocating "
             << n << " objects of "
             << n * sizeof(value_type)
             << " bytes. " << endl;
        delete[] reinterpret_cast<char *>(p);
    }
};

int main()
{
    string s;
    allocator<int> a;
    vector<int, CustomAlloc<int>> v;
    v.push_back(42);
    return 0;
}