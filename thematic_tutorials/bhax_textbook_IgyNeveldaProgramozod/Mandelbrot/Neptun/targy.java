
import java.util.*;


import java.util.*;


/**
 * Class targy
 */
public class targy {

  //
  // Fields
  //

  private String targykod;
  private String targynev;
  private int kapacitas;
  
  //
  // Constructors
  //
  public targy () { };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of targykod
   * @param newVar the new value of targykod
   */
  public void setTargykod (String newVar) {
    targykod = newVar;
  }

  /**
   * Get the value of targykod
   * @return the value of targykod
   */
  public String getTargykod () {
    return targykod;
  }

  /**
   * Set the value of targynev
   * @param newVar the new value of targynev
   */
  public void setTargynev (String newVar) {
    targynev = newVar;
  }

  /**
   * Get the value of targynev
   * @return the value of targynev
   */
  public String getTargynev () {
    return targynev;
  }

  /**
   * Set the value of kapacitas
   * @param newVar the new value of kapacitas
   */
  public void setKapacitas (int newVar) {
    kapacitas = newVar;
  }

  /**
   * Get the value of kapacitas
   * @return the value of kapacitas
   */
  public int getKapacitas () {
    return kapacitas;
  }



}
