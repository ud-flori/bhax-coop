import java.util.ArrayList;
import java.util.*;


/**
 * Class hallgato
 */
public class hallgato {

  //
  // Fields
  //

  private String nev;
  private String nk;
  private String szul_dat;
  private List<targy> targyak =  new ArrayList<>();

  
  //
  // Constructors
  //
  public hallgato () { };
  
  //
  // Methods
  //


  //
  // Accessor methods
  //

  /**
   * Set the value of nev
   * @param newVar the new value of nev
   */
  public void setNev (String newVar) {
    nev = newVar;
  }

  /**
   * Get the value of nev
   * @return the value of nev
   */
  public String getNev () {
    return nev;
  }

  /**
   * Set the value of nk
   * @param newVar the new value of nk
   */
  public void setNk (String newVar) {
    nk = newVar;
  }

  /**
   * Get the value of nk
   * @return the value of nk
   */
  public String getNk () {
    return nk;
  }

  /**
   * Set the value of szul_dat
   * @param newVar the new value of szul_dat
   */
  public void setSzul_dat (String newVar) {
    szul_dat = newVar;
  }

  /**
   * Get the value of szul_dat
   * @return the value of szul_dat
   */
  public String getSzul_dat () {
    return szul_dat;
  }


  public void targyhozzaad (targy targynev) {
    if(targynev.getKapacitas()!=0){
      targyak.add(targynev);
    }
  }

  /**
   * Get the value of targyak
   * @return the value of targyak
   */
  public List getTargyak () {
    return targyak;
  }





  //
  // Other methods
  //



}
