
#ifndef NEW_CLASS_H
#define NEW_CLASS_H


/**
  * class new_class
  * 
  */

class new_class
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  new_class();

  /**
   * Empty Destructor
   */
  virtual ~new_class();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //  


  // Protected attribute accessor methods
  //  


  // Protected attribute accessor methods
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  


  // Private attribute accessor methods
  //  


  // Private attribute accessor methods
  //

};

#endif // NEW_CLASS_H


#ifndef VEHICLE_H
#define VEHICLE_H


/**
  * class Vehicle
  * 
  */

class Vehicle
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  Vehicle();

  /**
   * Empty Destructor
   */
  virtual ~Vehicle();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //  



  /**
   */
  void start()
  {
  }


  /**
   */
  void breakDown()
  {
  }

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //  


  // Protected attribute accessor methods
  //  


  // Protected attribute accessor methods
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  

  int size;
  bool isBroken;
  double sizeOfEngine;
  Cars new_attribute;
  Buses new_attribute_1;

  // Private attribute accessor methods
  //  


  // Private attribute accessor methods
  //  


  /**
   * Set the value of size
   * @param value the new value of size
   */
  void setSize(int value)
  {
    size = value;
  }

  /**
   * Get the value of size
   * @return the value of size
   */
  int getSize()
  {
    return size;
  }

  /**
   * Set the value of isBroken
   * @param value the new value of isBroken
   */
  void setIsBroken(bool value)
  {
    isBroken = value;
  }

  /**
   * Get the value of isBroken
   * @return the value of isBroken
   */
  bool getIsBroken()
  {
    return isBroken;
  }

  /**
   * Set the value of sizeOfEngine
   * @param value the new value of sizeOfEngine
   */
  void setSizeOfEngine(double value)
  {
    sizeOfEngine = value;
  }

  /**
   * Get the value of sizeOfEngine
   * @return the value of sizeOfEngine
   */
  double getSizeOfEngine()
  {
    return sizeOfEngine;
  }

  /**
   * Set the value of new_attribute
   * @param value the new value of new_attribute
   */
  void setNew_attribute(Cars value)
  {
    new_attribute = value;
  }

  /**
   * Get the value of new_attribute
   * @return the value of new_attribute
   */
  Cars getNew_attribute()
  {
    return new_attribute;
  }

  /**
   * Set the value of new_attribute_1
   * @param value the new value of new_attribute_1
   */
  void setNew_attribute_1(Buses value)
  {
    new_attribute_1 = value;
  }

  /**
   * Get the value of new_attribute_1
   * @return the value of new_attribute_1
   */
  Buses getNew_attribute_1()
  {
    return new_attribute_1;
  }

  void initAttributes();

};

#endif // VEHICLE_H
