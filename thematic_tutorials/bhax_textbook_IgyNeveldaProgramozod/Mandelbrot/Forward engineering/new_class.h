
#ifndef NEW_CLASS_H
#define NEW_CLASS_H


/**
  * class new_class
  * 
  */

class new_class
{
public:
  // Constructors/Destructors
  //  


  /**
   * Empty Constructor
   */
  new_class();

  /**
   * Empty Destructor
   */
  virtual ~new_class();

  // Static Public attributes
  //  

  // Public attributes
  //  


  // Public attribute accessor methods
  //  


  // Public attribute accessor methods
  //

protected:
  // Static Protected attributes
  //  

  // Protected attributes
  //  


  // Protected attribute accessor methods
  //  


  // Protected attribute accessor methods
  //

private:
  // Static Private attributes
  //  

  // Private attributes
  //  


  // Private attribute accessor methods
  //  


  // Private attribute accessor methods
  //

};

#endif // NEW_CLASS_H
