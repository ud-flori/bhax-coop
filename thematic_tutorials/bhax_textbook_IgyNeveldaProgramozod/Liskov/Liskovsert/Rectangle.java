package Liskovsert;

public class Rectangle extends Shape {
    protected double width;
    protected double height;

    public double getWidth() {
        return width;
    }

    public void setWidth(double value) {
        this.width = value;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double value) {
        this.height = value;
    }

    public double area(double width, double height) {
        return width * height;
    }
}
