package EPAMLiskov;

public class Main {
    public static void main(String[] args) {
        Vehicle firstVehicle = new Supercar();
        firstVehicle.start();
        System.out.println(firstVehicle instanceof Car);
        Car secondVehicle = (Car) firstVehicle; // Car secondVehicle = new Supercar();
        secondVehicle.start();
        System.out.println(secondVehicle instanceof Supercar);
        Supercar thirdVehicle = new Vehicle(); // Vehicle is a parent, Supercar is
        // its child, so can't be an instance of that.
        thirdVehicle.start();
    }
}
