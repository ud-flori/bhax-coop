package EPAMLiskov;

public class Supercar extends Car {
    public Supercar() {
        System.out.println("Supercar");
    }

    @Override
    public void start() {
        System.out.println("Start [Supercar]");
    }
}
