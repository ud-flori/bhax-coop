package Szulogyerek;

public class Child extends Parent {
    protected String message;

    public Child() {
        message = "This is my message (child)";
        sendMessage(message);
    }

    public void sendMessage(String message) {
        System.out.println(message);
    }
}
