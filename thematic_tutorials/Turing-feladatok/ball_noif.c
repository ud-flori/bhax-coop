#include <stdio.h>
#include <curses.h>
#include <unistd.h>

//Fordításhoz -lncurses kacsoló

int main()
{
    WINDOW *ablak;
    ablak = initscr ();

    int x = 0;
    int y = 0; 
   
    int mx; 
    int my; 
    getmaxyx ( ablak, my , mx ); 
    int xnow = 1;
    int ynow = 1;
    
    int width[mx];
    int height[my];
    
    for(int i=0;i<mx;i++){
        width[i]=1;
    }

    for(int i=0;i<my;i++){
    height[i]=1;
    }
    width[0]=-1;
    height[0]=-1;
    width[mx-1]=-1;
    height[my-1]=-1;
    
    for ( ;; ) {

        
 
        mvprintw ( y, x, "0" );

        refresh (); 
        usleep( 100000 );
        clear();

        x = x + xnow; 
        y = y + ynow; 

        xnow*=width[x];
        ynow*=height[y];
    }

    return 0;
}

